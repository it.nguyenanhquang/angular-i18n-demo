import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public userName = 'Adam';
  public price = 7;
  public today = new Date();
  public subs = 0;
  public title = 'angular-i18n-demo';
  public sayHello = $localize`Hello ${this.userName}!!!`;

  public onSub() {
    this.subs++;
  }

  public switchLang(locale: 'en' | 'fr') {
    window.location.href = `/${locale}`;
  }
}

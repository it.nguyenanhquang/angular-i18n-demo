# AngularI18nDemo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.2.0.

## Demo Content

- Config `angular.json` for multiple locale setup and build
- Mark string to translate
- Generate translate file for each locale

## Development server

This demo has 2 different locales, `en` and `fr`.

- For standard English locale run `npm run start`
- For French language, run `npm run start:fr`

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

Run `npm run local:prod` will also build the project and start a simple node server to serve all the build of defined locales on localhost `http://localhost:8080`. (require expressJs)
const path = require('path');

const express = require('express');

const port = 8080;

const rootDir = path.join(__dirname, 'dist');

const locales = ['en', 'fr'];

const defaultLocale = locales[1];

const server = express();

// Serve static files (HTML, CSS, etc.)

server.use(express.static(rootDir));

locales.forEach((locale) => {
  server.get(`/${locale}/*`, (req, res) => {
    res.sendFile(
      path.resolve(rootDir, locale, 'index.html')
    );
  });
});

server.get('/', (req, res) =>
  res.redirect(`/${defaultLocale}`)
);

server.listen(port, () =>
  console.log(`App running at port ${port}…`)
);